import { DefaultFigSettings, DefaultDebugSettings } from './figTypes'


export type FigSettings = Partial<DefaultFigSettings>


export type DebugSettings = Partial<DefaultDebugSettings>

export type Settings = {
  figSettings: Partial<DefaultFigSettings>,
  debugSettings?: Partial<DefaultDebugSettings>
}

