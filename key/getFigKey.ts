
import { getKeyBase, getFigKeyData } from './keyFunctions'
import { KEY_CONSTANTS, FIG_CONSTANTS } from '../constants'

const { KEY_LENGTH } = KEY_CONSTANTS;

const getFigKey = (key: string) => {


  /// Get the number of different chars in the key
  /// This will vary depending on the format of the key)
  /// Only numbers gives only 10 possibilities etc
  const keyBase = getKeyBase(key)
  const { figBase, figKeyString } = getFigKeyData({ key, keyBase }) /* as GetFigKeyResult */
  const figKey = figKeyString.padStart(KEY_LENGTH, '0').split('')

  if (!figKey || !Array.isArray(figKey)) {
    throw Error('Missing or invalid fig key. FigKey was ' +
      JSON.stringify(figKey))
  }

  if (figKey.length !== KEY_LENGTH) {
    throw Error('Invalid fig key length. Got figKey length ' +
      figKey.length)
  }

  return {
    figKey,
    keyBase,
    figBase
  }

}

export default getFigKey
