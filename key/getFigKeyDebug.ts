
import { PipeData } from '../figTypes'
import { getKeyBase, getFigKeyData } from './keyFunctions'

const me = 'getFigKey'


const getFigKeyDebug = (data: PipeData) => {

  const {
    config: {
      figSettings: { key },
      debugSettings: { logInfo },
      CONSTANTS: {
        FIG_CONSTANTS: {
          MAX_FIG_BASE,
          MIN_FIG_BASE,
        },
        KEY_CONSTANTS: {
          KEY_BASES,
          KEY_LENGTH,
          SUPPORTED_CHARS,
        } },
    },
    setInfo,
  } = data

  const { log, myError, tearDown } = data.setup(me)

  // Log some info
  setInfo({ 'Input key': key })

  /// Get the number of different chars in the key
  /// This will vary depending on the format of the key)
  /// Only numbers gives only 10 possibilities etc
  const keyBase = getKeyBase(key)
  const { figBase, figKeyString } = getFigKeyData({ key, keyBase }) /* as GetFigKeyResult */
  const figKey = figKeyString.padStart(KEY_LENGTH, '0').split('')

  if (!figKey || !Array.isArray(figKey)) {
    throw myError('Missing or invalid fig key. FigKey was ' +
      JSON.stringify(figKey))
  }

  if (figKey.length !== KEY_LENGTH) {
    throw myError('Invalid fig key length. Got figKey length ' +
      figKey.length)
  }

  if (logInfo) {
    log.debug('Writing fig key based on key', key)
    log.debug('Using fig base', figBase)
    log.debug('Using', figKeyString.length, 'circles for core key of the available ', KEY_LENGTH)
  }

  // Add head to 0-padded FigKey

  /*  const figKey = */
  setInfo({
    'Fig key': figKeyString,
    'Fig base': figBase
  })

  data.keyData = {
    figKey,
    keyBase,
    figBase
  }

  tearDown()

  return data

}

export default getFigKeyDebug
