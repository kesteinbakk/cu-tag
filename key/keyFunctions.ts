import bigInt from 'big-integer'

import { KEY_CONSTANTS, FIG_CONSTANTS } from '../constants'

const { KEY_BASES, SUPPORTED_CHARS, KEY_LENGTH } = KEY_CONSTANTS;
const { MIN_FIG_BASE, MAX_FIG_BASE } = FIG_CONSTANTS

/// GetKeyBase: Returns the base of the key (how many different elements the key consists of)
export const getKeyBase = (key: string) => {


  const [int, hex, withOnlyNormalChars, withSpecialChars] = KEY_BASES

  const allChars = /[abcdefghijklmnopqrstuvwxyz]/
  const allCharRegex = new RegExp(allChars, 'g')

  const specialCharsString = Object.keys(SUPPORTED_CHARS).join('')
  const specialCharsRegex = new RegExp('[' + specialCharsString + ']', 'g')

  const noHex = /[ghijklmnopqrstuvwxyz]/
  const noHexRegex = new RegExp(noHex, 'g')

  const has = (regEx: RegExp) => key.search(regEx) > -1

  if (has(specialCharsRegex)) {
    // There special chars in the input
    return withSpecialChars
  }
  if (has(allCharRegex)) {
    // There are letters in the input
    if (!has(noHexRegex)) {
      // Only hex chars
      return hex
    }
    return withOnlyNormalChars
  }
  return int
}




// Get the fig key
type GetFigKeyArgs = {
  key: string,
  keyBase: number,
  coreKey?: string,
  figBase?: number
}
type GetFigKeyResult = { figBase: number, figKeyString: string }

export const getFigKeyData = (
  { key, keyBase, coreKey, figBase = MIN_FIG_BASE }: GetFigKeyArgs):
  GetFigKeyResult => {

  // Get key string. If string is too long for figure, try again with higher base

  coreKey = coreKey || getCoreKey(key, keyBase)


  const figKeyString = (() => {
    try {
      return bigInt(coreKey, keyBase).toString(figBase)
    } catch (err) {
      console.error('Invalid key: ', key)
      console.debug('Core key was', coreKey)
      throw Error('Key contains illegal characters: ' + err.message)
    }
  })()

  if (figKeyString.length > KEY_LENGTH) {

    // We do not have enough space in fig!

    if (++figBase > MAX_FIG_BASE) {

      // If base is above maxValue we don't have enough combinations to express the full base
      const errMsg = 'Invalid key. Above maximum data limit! (Fig base limit is ' +
        MAX_FIG_BASE + '. Length of the provided key in this base is ' + figKeyString.length +
        '. Space available in fig for key is ' + KEY_LENGTH + ')'

      throw Error(errMsg)

    }
    // Return self with higher base to reduce space needed
    return getFigKeyData({ key, keyBase, coreKey, figBase })

  }


  // All is good. Return the base and the figkey in the given base
  return {
    figBase,
    figKeyString,
  }


}

// Get the core key
function getCoreKey(key: string, keyBase: number) {

  const handleSpecialChars = (key: string) => Object.entries(SUPPORTED_CHARS).reduce((string, [key, val]) => {
    // Replace non alphabetic characters with code from settings
    return string.split(key).join(val)
  }, key)

  return keyBase > KEY_BASES[2] ? handleSpecialChars(key) : key
}