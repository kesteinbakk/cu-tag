
type LoggerFunc = (msg: any, ...optionalParams: any[]) => void
export type LoggerApi = {
  debug: LoggerFunc,
  info: LoggerFunc,
  warn: LoggerFunc,
  error: LoggerFunc,
  time: (name: string) => void,
  timeEnd: (name: string) => void,
  group: (name: string, exp: boolean) => void,
  groupCollapsed: (name: string) => void,
  groupEnd: () => void,
  clear: () => void,
  start: (name: string, exp: boolean) => void,
  end: (name: string) => void,
}

export type LoggerOptions = {
  log?: {
    error?: boolean,
    warn?: boolean,
    info?: boolean,
    debug?: boolean,
  }
}

export type MyLogger = (namespace: string, option?: LoggerOptions) => LoggerApi

const logger: MyLogger = (namespace, options = {}) => {

  namespace = namespace.length > 0 ? namespace + ': ' : ""

  const con = console || {}

  const api: LoggerApi = {
    error: function () {
      if (options.log?.error === false || !con.error) {
        return
      }
      const args = Array.from(arguments)
      if (namespace) {
        args.unshift(namespace)
      }
      con.error(...args)
    },

    warn: function () {
      if (options.log?.warn === false || !con.warn) {
        return
      }
      const args = Array.from(arguments)

      if (typeof args[0] === 'string') {
        args[0] = 'WARNING: ' + args[0]
      } else {
        args.unshift('WARNING:')
      }

      if (namespace) {
        args.unshift(namespace)
      }
      con.warn(...args)
    },

    debug: function () {
      if (options.log?.debug === false ||
        !(con.debug || con.info || con.log)) {
        return
      }
      const args = Array.from(arguments)
      if (typeof args[0] === 'string') {
        args[0] = 'DEBUG: ' + args[0]
      } else {
        args.unshift('DEBUG:')
      }

      if (namespace) {
        args.unshift(namespace)
      }
      const use = con.debug || con.info
      use(...args)

    },

    info: function () {
      if (options.log?.info === false || !(con.info || con.log)) {
        return
      }
      const args = Array.from(arguments)

      if (namespace) {
        args.unshift(namespace)
      }
      const use = con.info || con.log

      use(...args)

    },

    group: (name, expanded) => {
      if (!con.group) {
        return
      }
      if (expanded === false && con.groupCollapsed) {
        con.groupCollapsed(name)
        return
      }
      con.group(name)
    },

    groupCollapsed: (name) => api.group(name, false),

    groupEnd: () => {
      if (!con.groupEnd) {
        return
      }
      con.groupEnd()
    },

    time: (name) => {
      if (con.time) {
        con.time(name)
      }
    },

    timeEnd: (name) => {
      if (con.timeEnd) {
        con.timeEnd(name)
      }

    },

    clear: () => {
      if (con.clear) {
        con.clear()
      }
    },

    start: (name, expanded) => {
      api.group(name, expanded)
      api.time(name)
    },

    end: (name) => {
      api.timeEnd(name)
      api.groupEnd()
    }
  }


  return api as LoggerApi
} // logger


export default logger