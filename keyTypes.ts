
export type KeyBase = 10 | 16 | 36 | 46

export type KeyData = {
  figKey: string[], // The key to be used when generating the fig as an array
  keyBase: KeyBase // the number of elements the key consists of (see constans key bases)
  figBase: number // the base we need to use in the fig (the number of different sizes or colors) to contain the key
}