


import writeFigKey from '../mevemfig/key/writeFigKey'

import readFigKey from '../mevemfig/key/readFigKey'

//import setup from '../setup'

import {keyConstants} from '../constants'

import { getRandomInt } from '../utils'

const { SUPPORTED_CHARS }  = keyConstants

const noOfKeys = 2000

const lengthOfKeys = 25

const chooseFromChars = "0123456789abcdefghijklmnopqrstuvwxyz".split('')

const chooseFrom = [...chooseFromChars, ...Object.keys( SUPPORTED_CHARS) ];


test('Test if skipping key test', ()=>{
  expect(true).toBe(true)
})


describe.skip('Testing key generator', ()=> {

  //runTests()

})

function runTests(num = 0) {

  const key = genKey(lengthOfKeys)(chooseFrom)();

  if ( ! key || typeof key !== 'string' ) {

    throw new Error('Invalid random key for test:' + key);
  }

  // log.debug('Running new test no', options.done + 1);


  const keyData = writeFigKey( key )

  const readRes = readFigKey( keyData )

  test('Multiple key no' + num, ()=> {

    expect(readRes.key).toEqual(key)

  })

  if ( ++num >= noOfKeys ) {

    console.log('I have done ', noOfKeys, 'tests')
    return true

  } else {

    return runTests(num);

  }
}

function genKey(len) {

  if ( ! len || len < 2 ) {
    throw new Error('Invalid len for key generation');
  }

  return function(chooseFrom) {

    return function(string = "") {

      if ( string === "0") {
      // Cannot handle string starting with 0
        string = ""
      }

      if ( string.length < len ) {
        string += chooseFrom[getRandomInt(0, chooseFrom.length-1)];
        // log.debug('string is now', string, string.length);
        return genKey(len)(chooseFrom)(string)
      } else {
        //  log.debug('Finished generating random string', string);
        return string;
      }
    }
  }
}
