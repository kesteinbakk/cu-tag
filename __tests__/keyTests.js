
import bigInt from 'big-integer'

import toFig from '../key/getFigKey'

import fromFig from '../key/readFigKey'


//import calcKeyBase from '../key/calcKeyBase'
import { keySetup as setup } from '../createFig/setup'

import { keyConstants } from '../constants'

const logInfoToConsole = 0

const { KEY_BASES } = keyConstants

//const simple = { key:'1', str:'Simple test. Input key is 1', debug: true}
const number = { key: '1690', str: 'Testing only numeric key.' }
const hexa = { key: 'f1690', str: 'Testing hex key' }
const fullString = { key: 'fjdlavnohfejkfdn1937nvsk', str: 'Testing full string' }
const advanced = {
  key: JSON.stringify({ test: 1, val: 1988, a: 'c' }),
  str: 'Testing advanced key'
}

const maxLen = {
  key: 'abc123abc123abc123abc123abc123abc123abc123abc047',
  str: 'Testing maxlen'
}
/*const long = {
  key: 'lasfjdksavnwodas2224dsn21fdsagsdrersdsdvdfjg65654645gfdvdsvfd',
  str: 'Testing long char key'
}
*/
/*const failedKey = {
  key: 'vmdtyrmpf9ieenvzrir2lpl1r',
  str: 'Trying failed key'
}*/


describe('Testing individual key functions', () => {

  test('Core logic', () => {

    //const inputKey = '11223344123455'
    const [, , keyBase] = KEY_BASES
    const figBase = 4

    const coreKeyString = bigInt(fullString.key, keyBase).toString(figBase)
    const check = bigInt(coreKeyString, figBase).toString(keyBase)

    expect(fullString.key).toEqual(check)
  })


})


describe('Testing gen key and read key', () => {


  //keyTest(simple)

  keyTest(number)

  keyTest(hexa)

  keyTest(fullString)

  keyTest(advanced)

  expect(() => toFig(maxLen)).toThrow()

  //keyTest(long) Not supported in v 1

  //  keyTest(failedKey)

  //  keyTest("This is a test key")


})


function keyTest(use, onlyThis) {

  if (typeof use === 'string') {
    use = {
      key: use.toLowerCase(),
      str: "Customized key"
    }
  }

  if (onlyThis) {
    test.only(use.str, () => {

      doTests()

    })

  } else {
    test(use.str, () => {

      doTests()

    })
  }


  function doTests() {

    const toFigData = setup({ key: use.key, debug: use.debug })

    const keyData = toFig(toFigData)

    const { figKey, figBase, keyBase } = keyData


    if (logInfoToConsole) {
      console.info('Type', use.str)
      console.debug('Length:', use.key.length,)
      console.debug('KeyBase:', keyBase)
      console.log('Fig key input was:\n', figKey)

    }

    const toResData = setup({
      figKey,
      figBase,
      keyBase,
      debug: use.debug
    })


    const readRes = fromFig(toResData)

    expect(figKey.every((item) => item !== 'NaN' && item !== "")).toBe(true)


    expect(readRes.meta.keyBase).toEqual(keyBase)
    expect(readRes.meta.figBase).toEqual(figBase)


    expect(readRes.key).toEqual(use.key)


  }

}
