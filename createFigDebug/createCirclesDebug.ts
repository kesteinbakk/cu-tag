import { PipeData, CircleData, DebugData } from '../figTypes'

/*

Core fig functions for creating figure - javascript


 kesteinbakk

*/


const me = 'createCircles'

const createFig = (data: PipeData) => {

  const { log, myError, tearDown } = data.setup(me)

  if (!data.figData) { throw myError('Missing fig data for fig generation') }

  const {
    config: {
      figSettings: { size, angle },
      debugSettings: {
        logInfo,
        doChecks,
        kingControlThreshold,
        drawControlKings,
        drawDebugInfo,
        drawLegCenter,
        stopAfter,
        noOfLegs,
      },
      CONSTANTS: {
        FIG_CONSTANTS: { LEGS, MY_CONSTANT, PI, PId3 }
      }
    },
    setStatus,
    setInfo,
    match,
    figData: { factors },
  } = data


  const drawNoOfLegs = noOfLegs || LEGS

  const bigSize = size * MY_CONSTANT
  const centerSize = bigSize * MY_CONSTANT
  const origo = Math.round(size / 2)
  const center = {
    x: origo,
    y: origo,
    radius: centerSize
  }

  const circles: CircleData[] = []

  const debugData: DebugData = Object.seal({
    circles: [],
    kings: [],
    legEndInfo: [],
    controlKings: [],
    bigCircleCenters: [],
    steps: 0
  })

  data.debugData = debugData

  setInfo({
    'Center size': centerSize.toFixed(2),
    'Fig center x': center.x,
    'Fig center y': center.y,
    'Start angle': angle
  })

  if (!factors || !Array.isArray(factors)) {
    throw myError('Missing fig factors. Cannot generate fig.')
  }

  // Push center
  circles.push({ x: center.x, y: center.y, radius: centerSize })

  createLegs(angle)

  setStatus('Fig complete')

  setInfo({ 'No of circles': circles.length })

  data.figData.circles = circles

  tearDown()

  return data


  // Function handling leg creation
  function createLegs(angle: number, leg: number = 1): void {

    const x = center.x + (bigSize * Math.cos(angle))
    const y = center.y + (bigSize * Math.sin(angle))
    const legFactors = [...factors![leg - 1]]
    const legAngle = angle + MY_CONSTANT - PI
    // PI inverts leg center angle vs starting angle,
    // It does not matter if we add or remove PI in leg center or starting point of circles
    const legData = { x, y, legFactors, legAngle }

    // Mark center for the big leg circles
    if (drawLegCenter) {
      debugData.bigCircleCenters.push({ x, y, radius: centerSize * 0.3 })
    }

    // Plot circles to coordinates
    if (!createCircles(legData)) {
      // Circle creation stopped
      return
    }

    // Check if we are finished
    if (++leg > drawNoOfLegs) {

      if (doChecks) {
        // Get control circle at the end of each leg to check that this is same as next king
        checkControlKings()
      }

      return
    }

    // Move to next leg
    angle += PId3

    return createLegs(angle, leg)
  }


  // Function for creating circles in a leg
  function createCircles({
    x: legCenterX,
    y: legCenterY,
    legFactors,
    legAngle
  }: {
    x: number,
    y: number
    legFactors: number[],
    legAngle: number,
  }): boolean {

    // log.debug('factors', figFactors, 'angle', angle, 'center', center)
    // Get current size & angle factor


    // Get factor for current circle
    const factor = legFactors.shift()

    // Check if array was empty - if so we're finished with this leg
    if (!factor) {
      if (doChecks || drawControlKings) {
        // Store control kings as if we had continued leg with a center circle
        debugData.controlKings.push(getControlKing({
          x: legCenterX,
          y: legCenterY,
          angle: legAngle,
        }))

        // Store angle and legCenter in order to calculate excact diff with real king later
        debugData.legEndInfo.push({ legCenterX, legCenterY, legAngle })
      }
      return true
    }

    const angleFactor = factor * MY_CONSTANT

    // Push curve ahead equal the size-factor of this circle
    legAngle += angleFactor
    // Now curve is at center of the current small circle

    // Calculate coordinates along big circle curve and push to circles container
    circles.push({
      x: legCenterX + bigSize * Math.cos(legAngle),
      y: legCenterY + bigSize * Math.sin(legAngle),
      radius: factor * centerSize
    })

    // If debug, fetch debug info
    if (doChecks || drawControlKings || drawDebugInfo) {

      debugData.steps++
      const { x, y, radius } = circles[circles.length - 1]

      if (legFactors.length === 6) {
        // Store kings
        if (logInfo) {
          log.debug('Storing king: x:', x, 'y:', y)
        }
        debugData.kings.push({ x, y, radius, factor })
      }

      /*       if (logInfo) {
              log.debug(`Creating circle ${circles.length} (Size: ${radius})
              '(x,y,angle: ${x}, ${y}, ${legAngle})`)
            } */

      debugData.circles.push({
        //	legNo: leg,
        no: debugData.steps,
        x,
        y,
        radius,
        R: radius / centerSize,
        angle: legAngle,
      })

    }

    // Push z ahead equal radius of this circle,
    // Now curve is at end of circle
    legAngle += angleFactor

    if (stopAfter && stopAfter === circles.length) {
      log.info('Manually stopped fig gen on circle', stopAfter)
      return false
    }

    // Start over with next circle
    return createCircles({ x: legCenterX, y: legCenterY, legFactors, legAngle })

  }


  // Start logic


  // Debug functions

  function checkControlKings() {

    const { kings } = debugData


    debugData.legEndInfo.forEach((legData, index) => {
      const { legCenterX, legCenterY, legAngle } = legData
      const king = kings[index + 1] || kings[0]

      // Get precise position by using correct radius based on next kings actual size
      const control = getControlKing({
        x: legCenterX,
        y: legCenterY,
        angle: legAngle,
        factor: king.factor
      })

      const difX = king.x - control.x
      const difY = king.y - control.y
      const totalDif = difX + difY

      if (logInfo) {
        log.debug('King offset was', totalDif)
      }

      if (!match(totalDif, 0, kingControlThreshold, 1)) {
        log.error('Total dif was', totalDif, 'Above threshold of', kingControlThreshold)
        throw myError(`
          ControlKing check failed in leg ${debugData.kings.length}.
          Dif was above threshold. Total dif was ${totalDif}
        `)
      }

    })

  }


  function getControlKing({ x, y, angle, factor = 1 }: {
    x: number,
    y: number,
    angle: number,
    factor?: number
  }): CircleData {

    // Move forward the amount of angle
    const myCurve = angle + factor * MY_CONSTANT
    return {
      x: x + bigSize * Math.cos(myCurve),
      y: y + bigSize * Math.sin(myCurve),
      radius: centerSize * factor,
    }
  }

}
export default createFig