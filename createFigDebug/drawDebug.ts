/*

 Fig output when in browser environment using canvas - javascript

 Made by Karl Erik Steinbakk - kesteinbakk@gmail.com - 2018


*/

import { PipeData, CircleDataDebug, CircleData } from '../figTypes'

const me = 'draw'


const draw = (data: PipeData) => {

	const {
		config: {
			figSettings: {
				size,
				light,
				satur,
				color,
				border,
				colorKey,
			},
			debugSettings: {
				logInfo,
				doChecks,
				drawDebugInfo,
				drawControlKings,
				drawLegCenter,
				debugCircles,
				textColor,
			},
		},
		canvas,
		setStatus,
		figData,
		debugData,
		output
	} = data


	const { log, myError, tearDown } = data.setup(me)

	if (!figData) { throw myError('Missing fig data') }

	const { circles } = figData

	if (doChecks) {
		if (!Array.isArray(circles) || !circles.length) {
			log.error('Invalid circles', circles, ' in data obj', data)
			throw myError('Did not get circles data from figLogic')
		}
	}

	if (logInfo) {
		log.debug('Drawing fig using canvas.')
		log.debug('No of circles:', circles.length)
	}


	// Prepare canvas
	canvas.width = canvas.height = size

	const ctx = canvas.getContext('2d')

	if (!ctx) { throw myError('Failed to get context from canvas') }

	//ctx.clearRect(0, 0, size, size);

	// Draw circles on canvas
	circles.forEach((obj, index) => {

		if (doChecks && typeof (obj) !== 'object') {
			log.error('Invalid fig circle object for index', index)
			log.error(circles)
			throw myError('Invalid fig obj. Cannot draw index ' + index)
		}


		// Normal draw
		ctx.fillStyle = getCircleColor(index)
		ctx.beginPath()
		ctx.arc(obj.x, obj.y, obj.radius, 0, 2 * Math.PI)

		/*			var grd = ctx.createRadialGradient(x,y,r/2,x,y,r);
					grd.addColorStop(0,col);
					grd.addColorStop(1,"white");*/

		ctx.fill()


		/*		if (obj.borderCol) {
					ctx.strokeStyle = obj.borderCol;
					ctx.stroke();
				}*/

	}) // End logic data map

	// Draw debug info on canvas
	if (drawDebugInfo || drawControlKings || drawLegCenter) {

		if (!debugData) {
			throw myError('Missing debug data. But drawDebugInfo is true')
		}

		if (drawDebugInfo) {
			// Draw debug info inside circles
			ctx.strokeStyle = textColor
			//	ctx.font = '18px sans-serif'
			ctx.lineWidth = 1
			debugData.circles.forEach(obj => drawDebugInfoInsideCircles(ctx, obj))
		}

		if (drawControlKings) {
			// Draw extra debug circles
			ctx.strokeStyle = getHsl(debugCircles)
			ctx.lineWidth = 3
			debugData.controlKings.forEach(obj => drawDebugCircle(ctx, obj))
		}

		if (drawLegCenter) {
			ctx.strokeStyle = getHsl(debugCircles)
			ctx.lineWidth = 3
			debugData.bigCircleCenters.forEach(obj => drawDebugCircle(ctx, obj))
		}
	}

	if (border) {
		ctx.lineWidth = border
		ctx.strokeStyle = 'black'
		ctx.strokeRect(0, 0, size, size)
	}

	finalize()

	tearDown()


	return data

	function getCircleColor(index: number): string {

		let useColor: string | number = color // This is the default color

		// If we have a colorKey array, use color based on provided color key
		if (colorKey) {
			// Check if we have run out of colors
			if (colorKey[index] === undefined) {
				// Throw if doChecks else use default color
				if (doChecks) {
					log.error('Missing color key and doChecks. ColorKey is:', colorKey,
						'Circle index is', index)

					throw myError('Missing color for key no:' + index)
				}

			} else {
				// We have a provided color key
				useColor = colorKey[index]
			}
		}


		if (typeof useColor === 'number') {
			return getHsl({ color: useColor, satur, light })
		} else if (typeof useColor === 'string') {
			// Color is a string name of color
			return useColor
		} else {
			log.error('Invalid color: ', useColor, 'Color key was', colorKey)
			throw myError('Invalid color provided')
		}
	}

	function drawDebugCircle(ctx: CanvasRenderingContext2D, obj: CircleData) {
		ctx.beginPath()
		ctx.arc(obj.x, obj.y, obj.radius, 0, 2 * Math.PI)
		ctx.stroke()
	}


	function drawDebugInfoInsideCircles(
		ctx: CanvasRenderingContext2D,
		data: CircleDataDebug
	): void {

		const { no, x, y, R, } = data
		const offsetXNum = no > 9 ? -6 : -3
		ctx.strokeText(R.toFixed(2), x - 10, y)
		ctx.strokeText(no.toString(), x + offsetXNum, y + 10)

	}


	type GetHslArgs = { color: string | number, satur: string, light: string }

	function getHsl(circle: GetHslArgs): string {
		const { color, satur, light } = circle
		return `hsl(${color}, ${satur}, ${light}`
	}

	// Finalize data (timer etc)
	function finalize(): void {
		const { time } = output
		// Stop timer before printing debug marks
		time.end = +new Date()
		time.used = time.start ? ((time.end - time.start) / 1000).toFixed(3) + ' secs...' : 'No timing'

		output.uri = canvas.toDataURL()

		if (logInfo) {
			log.info(`Finished drawing fig. Time used: ${output.time.used}`)
		}

		setStatus('Finished! Time used: ' + time.used)
	}

}

export default draw

/*const insertInElement = ({element, dataUri})=> {

	if ( ! element ) {
		throw new Error('Need element to print fig to element. ')
	}

	if (typeof element === 'string') {
		element = document.getElementById(element);
	}

	if ( ! (element && element.nodeName)) {
		throw new Error('Missing figure element. Cannot draw image to screen');

	}

	//log.info('Creating image element in', element);
	var img = document.createElement('img');
	img.setAttribute('src', dataUri);
	element.appendChild(img);


}


const printToCanvas = ( {canvas, element} ) => {

	if ( ! canvas ) {
		throw new Error('Need canvas to print to canvas.')
	}

	if (typeof element === 'string') {
		element = document.getElementById(element);
	}

	if ( ! (element && element.nodeName)) {
		throw new Error('Missing figure element. Cannot draw image to screen');
	}

	if (element.nodeName.toLowerCase() !== 'canvas') {
		throw new Error('Cannot draw to canvas. No canvas element passed in as element.');
	}

	//log.info('Drawing on canvas');
	element.height = canvas.height;
	element.width = canvas.width;
	var ctx = element.getContext("2d");
	ctx.drawImage(canvas, 0, 0);

}*/
