
import logger from '../logger'


type MarkArgument = {
  x: number,
  y: number,
  type: string,
  size?: number,
}

type MarkData = {
  color: string | number,
  x: number,
  y: number,
  size: number,
  type: string,
}

const markSettings: { [key: string]: any } = {

  paths: true,
  pathsLast: true,
  prefEndPoints: true,
  filteredEndPoints: true,
  points: true,
  centers: true,
  kings: true,
  errors: true,
  possibleCenters: true,
  failedCenterChecks: true,
  types: {
    errors: {
      color: 'red',
      size: 6
    },
    errorInfo: {
      color: 'darkRed',
      size: 4
    },
    next: {
      size: 3,
      color: 'yellow',
    },
    nextNoHit: {
      size: 3,
      color: 'red',
    },
    medianData: {
      size: 5,
      color: 'darkGreen',
    },
    offset: {
      size: 7,
      color: 'darkBlue',
    },
    startPos: {
      size: 12,
      color: 'darkGreen'
    },
    detectNoHit: {
      color: 'purple',
      size: 3
    },
    detectHit: {
      color: 'lime',
      size: 20
    },
    center: {
      color: 'black',
      size: 25
    },
    centers: {
      color: 'darkBlue',
      size: 8
    },
    matchedCenters: {
      color: 'lightBlue',
      size: 3
    },
    paths: {
      size: 0,
      color: 'green'
    },
    pathsLast: {
      size: 3,
      color: 'black'
    },
    prefEndPoints: {
      size: 5,
      color: 'darkBrown'
    },
    legCenters: {
      size: 30,
      color: 'olive', // Note: markHits will overwrite this if both run
    },
    points: {
      color: 'purple',
      size: 5
    },
    conPaths: [
      {
        size: 0,
        color: 'pink'
      },
      {
        size: 0,
        color: 'blue'
      },
      {
        size: 0,
        color: 'yellow'
      },
      {
        size: 0,
        color: 'black}'
      },
    ],

    kings: {
      size: 15,
      color: 'cyan',
    },
  } // types
}


const log = logger('markFig')

const marks: MarkData[] = []


// Functions for analyzing fig
export const mark = (data: MarkArgument) => {

  const { x, y, type } = data

  if (markSettings[type] === false) { return false }

  const set = markSettings.types[type]

  if (!set) {

    throw new Error('I have no info on markType ' + type)

  }

  const { color = 'blue' } = set
  const size = !isNaN(set.size) ? set.size : data.size || 0

  marks.push({ color, x, y, size, type })

}

export const clearMarks = () => {

  marks.length = 0

}


export const printMark = (image: any) => {

  //log.debug("PRINTING IMAGE MARKS", marks)

  type RecArg = { color: string | number, x: number, y: number, size: number }

  const rec = ({ color, x, y, size }: RecArg) => {

    if (size <= -1) {

      // We're finished
      return

    }
    //log.debug('setting pix color ', x, y, color)
    image.setPixelColor({ color, x: x - size, y })
    image.setPixelColor({ color, x: x + size, y })
    image.setPixelColor({ color, x, y: y - size })
    image.setPixelColor({ color, x, y: y + size })

    size--
    rec({ color, x, y, size })

  }

  marks.forEach((mark, index) => {

    const { size, x, y, type } = mark

    if (isNaN(size) || size < 0) {

      log.error('Invalid size', size, 'From', type)
      throw new Error('Invalid size for image mark')

    }

    if (isNaN(x + y)) {

      log.error('Invalid x, y', x, y, 'From mark type', type, 'Data received was', mark, 'Last valid mark was', marks[index - 1],)
      throw new Error('Invalid x/y for image mark. Type was: ' + type)

    }

    return rec({
      ...mark,
      size: size * image.sizeLevel,

    })

  })

  // Reset mark array
  marks.length = 0

}

