import getNext from './getNext'

import getCircleData from '../circleData/getCircleData'

//import getCirclePaths from '../circleData/getCirclePaths'

const me = 'getCenter'


const getCenter = (data) => {


  const {

    markFig: { mark, clearMarks },
    utils: { getMin, getOffsetPoint, match /*getCenterFromPoints*/ },
    startPos,
    isHit,
  } = data


  const {

    [me]: {
      radiusIncFactorNormal,
      radiusIncFactorForKing,
      kingOffsetFactor,
      kingCenterCheckThreshold
    },
    debug,
    //  constants: { /*PId2, PId3,*/ PId6 }

  } = data.input


  const { myError, log, tearDown } = data.setup(me)


  const getMedianOfData = (data) => {

    if (!data) {

      throw myError('Cannot get median without input data')

    }

    let medianData

    if (data.length === 1) {

      medianData = data[0]

    } else {

      // Get middle
      const mid = Math.floor(data.length / 2);
      const notEven = data.length % 2

      if (notEven) {

        medianData = { ...data[mid] }

      } else {

        // Even length:
        const d1 = data[mid - 1]
        const d2 = data[mid]

        // Get average from two mid items

        medianData = {
          x: (d1.x + d2.x) / 2,
          y: (d1.y + d2.y) / 2,
          angle: (d1.angle + d2.angle) / 2
        }

      }

    }

    if (debug) {

      if (!medianData) {

        throw myError('Failed to get medianData')

      }

      log.debug('Got median data', medianData)
      mark({ ...medianData, type: 'medianData' })

    }


    return medianData

  }


  const getPostKingConnection = (next) => {


    if (debug) {

      log.debug('Searching for king connection. ')

    }


    const smallest = next.reduce((acc, nextSet, index) => {

      if (nextSet.length < acc.length) {

        acc = nextSet

      }

      return acc

    }).length

    //log.debug('Smallest was', smallest)


    return next.reduce((acc, current, index) => {


      if (current.length === smallest) {

        if (acc.value) {

          throw myError('We have 2 sml circles in king. Cannot get correct kingset')

        }

        if (acc.bigs.length) {

          // We come from bigger
          acc.value = acc.bigs.pop()


        } else {

          // Sml is first
          acc.sml = true

        }

        return acc

      }


      acc.bigs.push(current)


      if (index === 2 && acc.sml) {

        // If sml was first
        acc.value = current

      }

      return acc

    }, { bigs: [] }).value

  }


  const getHighLevelSet = (next) => {

    const highLevelSet = next.filter(set => set.length > 6)

    if (!highLevelSet.length) {

      // No king or center
      return false

    }

    if (highLevelSet.length > 1) {

      throw myError('Multiple highLevelSets')

    }


    if (debug) {

      log.debug('Detected high level next set')

    }

    return getMedianOfData(highLevelSet[0])

  }


  const kingAndCenterCheck = (angle, circle) => {


    // Get offset from last circle, based on high level set angle to get close to king

    const offset = circle.radius + circle.radius * kingOffsetFactor

    const preOffset = getOffsetPoint({ ...circle, angle }, offset)

    if (debug) {

      //log.debug('Offset point is', point)
      mark({ ...preOffset, type: 'offset' })

    }

    if (!isHit(preOffset)) {

      if (debug) {

        log.debug('Pre offset point was not a hit')

      }

      return {}

    }

    const king = getCircleData(data, preOffset)

    if (
      !match(king.x, preOffset.x, kingCenterCheckThreshold) ||
      !match(king.y, preOffset.y, kingCenterCheckThreshold)
    ) {

      throw myError('Failed to match king center with offsetPoint')

    }

    // Get next data just outside king/center ( complete circle )


    const nextTest = getNext(data, {
      ...king,
      inc: radiusIncFactorForKing,
      angle: false
    })


    if (nextTest.length === 6) {

      if (debug) {

        log.debug('Possible center', nextTest)

      }

      const circCheck = nextTest.some(circSet => circSet.length > 3)

      if (circCheck) {

        throw myError('Got center, but failed individual size checks. Length above 3.')

      }


      king.angles = nextTest.map(set => getMedianOfData(set).angle)

      if (debug) {

        mark({ ...king, type: 'center' })

      }

      return { center: king }

    }


    if (nextTest.length === 3) {

      if (debug) {

        log.debug('Possible king', nextTest)

      }


      const kingSet = getPostKingConnection(nextTest)

      if (!kingSet || kingSet.length < 3) {

        throw myError('Failed in king test. King connection set is missing or too small', kingSet)

      }


      if (debug) {

        log.debug('Confirmed KING')
        mark({ ...king, type: 'kings' })

      }

      //log.debug('Got king data', kingData)

      const kingConnection = getMedianOfData(kingSet)

      const offset = king.radius + king.radius * radiusIncFactorNormal
      king.angle = kingConnection.angle

      const postOffset = getOffsetPoint(king, offset)

      if (!isHit(postOffset)) {

        throw myError('Post offset point was not a hit')

      }

      if (debug) {

        //log.debug('Offset point is', point)
        mark({ ...postOffset, type: 'offset' })

      }

      const nextCircle = getCircleData(data, postOffset)


      return { king: nextCircle }

    }

    // No king or center match
    return {}

  }


  const step = (circle) => {


    if (debug) {

      if (++data.output.usedTicks === data.input.maxTicks) {

        throw myError('Max number of ticks reached')

      }

    }


    if (!circle.inc) {

      circle.inc = radiusIncFactorNormal

    }

    const next = getNext(data, circle)

    log.debug('Analyzing next', next)


    // Check for king or center connection


    const highLevelSet = getHighLevelSet(next)


    if (highLevelSet) {

      if (data.input.clearMarks) {

        clearMarks()

      }


      const check = kingAndCenterCheck(highLevelSet.angle, circle)


      if (check.king) {

        return step(check.king)

      }

      if (check.center) {

        return check.center

      }

    }


    const len = next.length


    if (!circle.angle) {


      if (len === 1) {

        throw myError('Got only one next hit on circle with no angle')

      }

      // No angle - go for the smallest above 2

      const circles = next.reduce((acc, set) => {

        if (set.length > 2 && set.length < 7) {

          const setData = getMedianOfData(set)

          acc.push(getCircleData(data, setData))

        }

        return acc

      }, [])

      if (!circles.length) {

        log.error('Next sets', next)
        throw myError('Failed to find any valid next sets')

      }

      const smallest = getMin(circles, 'radius')

      if (debug) {

        log.debug('I have no angle. Using circle', smallest, ' out of circles ', circles)

      }


      return step(smallest)

    }


    // Step with angle:

    if (len !== 1) {

      log.error('Data sets', next)

      throw myError('Got multiple data sets when expecting only one. Had angle from prev circle')

    }

    // Normal leg with one set, just continue

    const nextData = getMedianOfData(next[0])

    const nextCircle = getCircleData(data, nextData)

    if (nextCircle.radius > circle.radius) {

      throw myError('Going in the wrong direction')

    }

    if (debug) {

      log.debug('Normal leg')

    }

    return step(nextCircle)


  }


  // Core logic

  const startCircle = getCircleData(data, startPos)

  const center = step(startCircle)

  if (debug) {

    log.info('Got center', center)

  }


  data.center = center


  tearDown()

  return center

}

export default getCenter

/*while ( kings.length < 1 && counter < 100 ) {

  counter++

  if ( debug ) {
    log.debug('Trying to get kings. Test no', counter)
  }

  const curKing = kings[kings.length-1]


  const angle = curKing ? curKing.angle : undefined

//    kings.push( getKing( data, circle, angle ) )

}

if ( kings.length < 3 ) {
  throw myError('Failed to get 3 kings')
}

const center = getCenterFromPoints( ...kings )

if ( debug ) {
  mark({...center, type: 'center'}, )
}
*/


/*       // const king = getCircleData(data, circle)
        const circleSet = getMedianOfDataSets(next)

        const dif1 = angleDif(circleSet[0].angle, circleSet[1].angle  )

        const dif2 = angleDif(circleSet[0].angle, circleSet[2].angle)

        log.debug('Dif1, dif2', dif1, dif2)

        const nextCircle = dif1 < dif2 ? circleSet[1]: circleSet[2]

        nextCircle.fromKing = true*/
