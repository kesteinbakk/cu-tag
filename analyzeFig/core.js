
/*

 Init analyze- javascript

 Searching for the figure and getting data for the "search" object

 kesteinbakk

*/


//import initWorker from './webWorker'

import { getRandomInt } from '../utils'

import getCenter from './centerData/getCenter.js'

//import getKingsData from './figData/getKingsData.js'

import getKeyData from './figData/getKeyData.js'

const me = 'core'


const core = (data) => {

  console.debug('INPUT DATA IS', data)


  const {

    image,
    markFig: { mark },
    setInfo,
    isHit

  } = data

  const {

    [me]: {
      noOfTests
    },

    // constants: { THE_MEVEM_VALUE, PI },
    freeze,

    debug,


  } = data.input

  const { log, myError, tearDown } = data.setup(me)

  const { size } = image


  /*
    const inc = Math.PI/2
  */


  let counter = 0


  //  Fig:

  if (debug) {

    if (data.input.fakeCenter) {

      data.center = data.input.fakeCenter
      data.center.faked = true
      setInfo('Center faked', true)
      log.info('Faking center')

    }

  }

  const stopAt = freeze ? 1 : noOfTests

  while (!(data.center || counter === stopAt)) {

    counter++

    if (debug) {

      log.debug('Search test no', counter)

      if (freeze) {

        log.debug('Using freezed position', freeze)

      }

    }

    const x = freeze ? freeze.x : getRandomInt(0, size)
    const y = freeze ? freeze.y : getRandomInt(0, size)


    if (isHit({ x, y })) {


      if (debug) {

        log.debug('Got hit (x,y):', x, y, ' on try', counter)

        mark({ x, y, type: 'detectHit' })

      }

      setInfo({
        'Start hit': { x, y }
      })

      data.startPos = { x, y } // Used by freeze ui

      const center = getCenter(data)

      if (!center) {

        throw myError('Failed to find center')

      }

      data.center = center


      //data.finished = true
      break;

    } else if (freeze) {

      throw myError('Freezed point was not a hit')

    }


  } // loop


  if (!data.center) {

    log.error('Exited loop, but no center found')

    throw myError('Failed to find center')

  }


  setInfo({
    'Analyzed fig size:': data.figSize,
    'Center size': data.center.radius.toFixed(2),
    'No of hits': counter
  })

  data.key = getKeyData(data)

  setInfo({
    'Got factors': !!data.factors.length
  })


  //stop('End core')
  tearDown()


  return data

}

export default core


/*  const findFig = () => new Promise( (resolve, reject) => {


    if ( window.Worker ) {
      const worker = new Worker(initWorker)

      worker.onmessage = e=>{
        log.info('MSG FROM WORKER', e.data)
        resolve(e.data)
      }


      worker.postMessage({
        test:'hello'
      })

    } else {
      throw new Error('No worker support')
    }


  })


  const res = await findFig()*/

  //data.status = res
