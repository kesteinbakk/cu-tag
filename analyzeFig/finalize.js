

/*

 Public Analyze Functions - javascript

 Made by Karl Erik Steinbakk - kesteinbakk@gmail.com - 2016

*/


const finalize = (data) => {

  const {
    markFig: { printMark },
    image,
    setInfo,
    setStatus,
    setup,
  } = data


  const {
    imgType,
    debug,
  } = data.input;


  const {
    time,
    hasError
  } = data.output


  const me = 'Finalize'

  const { log, tearDown } = setup(me)


  // Stop timer before printing debug marks
  time.end = +new Date();

  time.used = time.start ? ((time.end - time.start) / 1000).toFixed(3) + ' secs...' : 'No timing';

  if (debug) {

    log.info(`Finished generating or analyzing. Used: ${time.used}`)

  }

  setStatus('Finished! Time used: ' + time.used)

  const noOfErrors = hasError()

  setInfo('Time used', time.used)


  if (noOfErrors) {

    log.error('Error in process', noOfErrors)
    setInfo({ 'No of errors': noOfErrors })

  }


  if (image) {

    setInfo({
      'Image size': image.size,
      'Pixels': (image.size ** 2) / 1000000 + 'MP'

    })

    if (debug) {

      try {

        printMark(image)

      } catch (err) {

        const msg = 'Failed to print image marks. Err msg: ' + err.message

        log.error(msg)

        setStatus(msg)

      }

    }

    if (image.canvas) {

      // Get fig uri (must be done after marking fig)
      const uri = image.canvas.toDataURL(imgType)

      if (!uri) {

        log.error('Did not mange to get uri for fig. Canvas was', typeof canvas)
        log.error('Data was', data)

      }

      data.output.uri = uri

    } else {

      log.error('Got image, but no canvas there')

    }


  } else {

    log.info('No image in data')

  }


  tearDown()

  return data

}

export default finalize
