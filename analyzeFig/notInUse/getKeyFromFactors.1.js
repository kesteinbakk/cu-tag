

const me = 'getKeyFromFactors'


const getKeyFromFactors = (data) => {


  const {
    setInfo,

  } = data


  const {

    debug,
    //  [me] : { maxOffLevel },
    keyConstants: { KEY_BASES, BASE_FACTORS: { INNER, OUTER } },

  } = data.input

  const factors = data.factors || data.input.factors // (from input if key is used direct)

  const { log, myError, tearDown } = data.setup(me)


  const sortLegFactors = () => {

    const start = factors.reduce((acc, legFactor, index) => {

      const king = legFactor[6]

      if (king > acc.size) {

        acc.size = king
        acc.index = index

      }
      return acc

    }, {
      size: 0,
      index: undefined
    })

    if (debug) {

      log.debug('Start index is', start.index)
      log.debug('Input factors', [...factors])

    }
    setInfo({
      'start index': start.index
    })

    while (start.index) {

      factors.push(factors.shift())
      start.index--

    }

    if (debug) {

      log.debug('Sorted figFactors', factors)

    }

  }

  const getMeta = () => factors.map((legFactors, index) => {


    return legFactors[6] - 1

  })

  const splitFactors = () => {

    const getIncsReducer = (bases) => (acc, factor, index) => {

      const inc = factor - bases[index] + acc.offset


      acc.offset = inc
      acc.vals.push(inc < 0 ? 0 : inc)

      return acc

    }


    return factors.reduce((acc, legFactors, index) => {

      if (debug) {

        setInfo('Read legFactors for leg ' + index, legFactors.map(val => val.toFixed(3)))

      }

      const innerFactors = legFactors.slice(0, 5)
      const outerFactors = legFactors.slice(7, 12)

      const innerIncs = innerFactors.reduce(getIncsReducer(INNER), { vals: [], offset: 0 })
      const outerIncs = outerFactors.reduce(getIncsReducer(OUTER), { vals: [], offset: 0 })
      acc.push(...innerIncs.vals, ...outerIncs.vals)

      if (innerIncs.negative || outerIncs.negative) {

        log.error('Failed negative leg index:', index, 'Values', acc)
        throw myError('Negative values detected')

      }

      return acc

    }, [])

  }


  /*LOGIC*/


  if (!Array.isArray(factors) || factors.length !== 6) {

    log.error('Invalid fig factors', factors)
    throw myError('Missing or invalid figFactors. Connot get key', data)

  }

  sortLegFactors()

  const meta = getMeta()

  if (debug) {

    log.debug('META IS', meta)

  }

  setInfo('metaFactors', meta.map(val => val.toFixed(3)))


  const [figBase1, kb1, step1, figBase2, kb2, step2] = meta

  const step = (step1 + step2) / 2

  const figBase = Math.round(((figBase1 + figBase2) / 2) / step)

  const keyBaseIndex = Math.round((kb1 + kb2) / step)

  const baseDif = Math.abs(figBase1 - figBase2)

  const stepDif = Math.abs(step1 - step2)

  setInfo({
    'Fig base': figBase,
    'Keybase index': keyBaseIndex,
    'Off level': (baseDif + stepDif).toFixed(3)
  })

  /*  if ( baseDif > maxOffLevel ) {
      log.error('To high diff between base values:', figBase1, figBase2)
      throw myError('Diff too high on base values', data, me)
    }


    if ( stepDif > maxOffLevel ) {
      log.error('To high diff between step values:', step1, step2)
      throw myError('Diff too high on step values', data, me)
    }*/


  const keyBase = KEY_BASES[keyBaseIndex]

  if (typeof keyBase !== 'number' || keyBase < 10) {

    log.error('Invalid key base', keyBase)
    throw myError('Invalid key base', data)

  }


  //  const stepCenter = Math.ceil( (figBase-1) / 2 )

  if (debug) {

    log.debug('figbase is ', figBase)
    log.debug('KB', keyBaseIndex)
    log.debug('step was', step)
    //    log.debug("stepCenter", stepCenter)

  }


  const splittedFactors = splitFactors()

  log.debug('Splitted factors are', splittedFactors)

  const figKey = splittedFactors.map(factor => Math.round(factor / step))

  setInfo({
    'Fig key': figKey,
    'Fig base': figBase
  })

  if (debug) {

    log.debug('Got figkey', figKey)

  }

  data.figKey = figKey

  data.figBase = figBase

  data.keyBase = keyBase

  tearDown()

  return data

}

export default getKeyFromFactors